Set `PGAPPNAME` variable to log the name of the user of psql:

```
export PGAPPNAME="psql_$(logname 2>/dev/null || ps -o user -q $$ --no-heading)"
```

Explanation:
Use "logname" for users who've logged in, and fall back to ps for users
running "psql" via non-interactive login sessions (as via `ssh db psql ...`).
