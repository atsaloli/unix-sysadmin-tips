```
Subject: Re: [HTSQL-users] NEW TO HTSQL NEED HELP

for HTSQL, tables must have primary keys and foreign keys

Kirill wrote:

To use HTSQL, you really need primary, unique and foreign keys defined 
for all your tables.  If your database does not define foreign keys, you 
could define them manually using `tweak.override` addon.


On Wed, Jan 2, 2013 at 8:57 AM Kirill Simonov <xi@resolvent.net> wrote:
Jiten,

It works for me:

$ cat jiten_test.yaml
htsql:
   db: mysql:jiten_test
tweak.override:
   foreign-keys:
     - mst_wrkemp(wrkemp_id) -> tran_wrkemp_jr(wrkemp_id)

$ htsql-ctl shell -C jiten_test.yaml
Type 'help' for more information, 'exit' to quit the shell.
jiten_test$ /tran_wrkemp_jr{*, mst_wrkemp.*}?mst_wrkemp.wrkemp_name~'jit'
Expected a singular expression
While translating:
     /tran_wrkemp_jr{*, mst_wrkemp.*}?mst_wrkemp.wrkemp_name~'jit'
                                                 ^^^^^^^^^^^
Well, the query itself fails because the primary keys are not 
configured, however the `tweak.override` foreign key definition works fine.


Thanks,
Kirill

On 01/02/2013 04:17 AM, jiten wrote:
> I have created new database and table for testing please check sql dump.
>
> Jiten
>
>
>
>
>
> On Wed, 02 Jan 2013 03:17:09 -0600
> Kirill Simonov <xi@resolvent.net> wrote:
>
>> On 01/02/2013 03:11 AM, jiten wrote:
>>> I have tried with different database also but same error
>>> message. anyway no problem thanks for support.
>>>
>>
>> I might be able to help if you could share a dump of the database or,
>> at least, of the database schema.
>>
>> Thanks,
>> Kirill
>>

_______________________________________________
HTSQL-users mailing list
HTSQL-users@lists.htsql.org
http://lists.htsql.org/mailman/listinfo/htsql-users


```
