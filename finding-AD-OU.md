# Finding what AD OU a host is in

Author: Aleksey Tsalolikhin  
Date: 16 Aug 2018  

To find out which AD OU container a host is in,
run `adinfo` with `-V` (verbose) option and look
for `OU=`.

```
# adinfo -V 2>&1 | grep OU=
  Found computer account: CN=zeus,OU=xxx,OU=yyy,OU=zz,DC=example,DC=com
#
```

Note: `adinfo` writes _everything_ to STDERR, so I redirect STDERR to STDOUT so I can grep out the OU string.

P.S. Here are even better ways:

    adquery user -D `hostname -s`
    adquery user -C `hostname -s`
    